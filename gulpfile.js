var gulp = require('gulp'),
        sass = require('gulp-ruby-sass'),
        autoprefixer = require('gulp-autoprefixer'),
        uglify = require('gulp-uglify'),
        rename = require('gulp-rename'),
        coffee = require('gulp-coffee'),
        cleanCSS = require('gulp-clean-css'),
        compass = require('gulp-compass');

gulp.task('express', function() {
  var express = require('express');
  var app = express();
  app.use(require('connect-livereload')({port: 35729}));
  app.use(express.static(__dirname));
  app.listen(4000, '0.0.0.0');
});

var tinylr;
gulp.task('livereload', function() {
  tinylr = require('tiny-lr')();
    tinylr.listen(35729);
});

function notifyLiveReload(event) {
  var fileName = require('path').relative(__dirname, event.path);

  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}

gulp.task('coffee', function(){
    return gulp.src('js/*.coffee')
        .pipe(coffee({
            bare: true
        }))
        .pipe(gulp.dest('js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({
            mangle: true
        }))
        .pipe(gulp.dest('js'));
});

gulp.task('compass', function() {
    return gulp.src('sass/app.scss')
        .pipe(compass({
            css: 'css',
            sass: 'sass',
            image: 'img'
        }))
        .pipe(autoprefixer({
            browsers: ['IE 7'],
			cascade: false
        }))
        .pipe(gulp.dest('css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
    gulp.watch('sass/*.scss', ['compass']);
    gulp.watch('js/*.coffee', ['coffee']);
    gulp.watch('*.html', notifyLiveReload);
    gulp.watch('css/*.css', notifyLiveReload);
});

gulp.task('default', ['coffee', 'compass', 'express', 'livereload', 'watch'], function() {

});
